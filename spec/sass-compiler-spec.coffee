SassCompiler = require '../lib/sass-fast-compiler'

describe "SassCompiler", ->
  [workspaceElement, activationPromise] = []

  beforeEach ->
    workspaceElement = atom.views.getView(atom.workspace)
    activationPromise = atom.packages.activatePackage('sass-compiler')
